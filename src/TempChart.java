
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.Timer;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.DynamicTimeSeriesCollection;
import org.jfree.data.time.Second;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

/** @see http://stackoverflow.com/questions/5048852 */
public class TempChart extends ApplicationFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String TITLE = "Room Temperature";
	private static final String START = "Start";
	private static final String STOP = "Stop";
	private static final float MINMAX = 50;
	private static final int COUNT = 2 * 60;
	private static final int FAST = 1000;
	private static final int SLOW = FAST * 5;
	private Timer timer;
	private float[] newData = new float[1];
	String temperature = "0";

	public TempChart(final String title) {
		super(title);
		Calendar cal = Calendar.getInstance();
		Date d = cal.getTime();
		newData[0] = 0;
		
		final DynamicTimeSeriesCollection dataset = new DynamicTimeSeriesCollection(
				1, COUNT, new Second());
		dataset.setTimeBase(new Second(cal.get(Calendar.SECOND), cal.get(Calendar.MINUTE), cal.get(Calendar.HOUR), cal.get(Calendar.DATE), cal.get(Calendar.MONTH), cal.get(Calendar.YEAR)));
		
		dataset.addSeries(temperatureData(), 0, "Temperature");
		JFreeChart chart = createChart(dataset);

		final JButton run = new JButton(STOP);
		run.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String cmd = e.getActionCommand();
				if (STOP.equals(cmd)) {
					timer.stop();
					run.setText(START);
				} else {
					timer.start();
					run.setText(STOP);
				}
			}
		});

		final JComboBox<String> combo = new JComboBox<String>();
		combo.addItem("Fast");
		combo.addItem("Slow");
		combo.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if ("Fast".equals(combo.getSelectedItem())) {
					timer.setDelay(FAST);
				} else {
					timer.setDelay(SLOW);
				}
			}
		});

		this.add(new ChartPanel(chart), BorderLayout.CENTER);
		JPanel btnPanel = new JPanel(new FlowLayout());
		btnPanel.add(run);
		btnPanel.add(combo);
		this.add(btnPanel, BorderLayout.SOUTH);

		timer = new Timer(FAST, new ActionListener() {


			@Override
			public void actionPerformed(ActionEvent e) {
				//newData[0] = Calendar.getInstance().getTimeInMillis() / 10e10f;
				Runtime r = Runtime.getRuntime();
				Process p;
				String line = "";
				boolean err = false;
				try {
					p = r.exec("/usr/local/bin/pcsensor");
					BufferedReader input = new BufferedReader(
							new InputStreamReader(p.getInputStream()));

					while ((line = input.readLine()) != null) {
						temperature = line.substring(line.length() - 6,
								line.length() - 1);
						try {
							newData[0] = Float.valueOf(temperature);
							err = false;
						} catch (NumberFormatException e1) {
							err = true;
						}
						System.out.println(temperature);
					}
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				if (!err) {
					dataset.advanceTime();
					dataset.appendData(newData);
				}
			}
		});
	}

	private float[] temperatureData() {
		float[] a = new float[COUNT];
		for (int i = 0; i < a.length; i++) {
			a[i] = 0;
		}
		return a;
	}

	private JFreeChart createChart(final XYDataset dataset) {
		final JFreeChart result = ChartFactory.createTimeSeriesChart(TITLE,
				"hh:mm:ss", "degrees Celsius", dataset, true, true, false);
		final XYPlot plot = result.getXYPlot();
		ValueAxis domain = plot.getDomainAxis();
		domain.setAutoRange(true);
		ValueAxis range = plot.getRangeAxis();
		range.setRange(-MINMAX, MINMAX);
		return result;
	}

	public void start() {
		timer.start();
	}

	public static void main(final String[] args) {
		EventQueue.invokeLater(new Runnable() {

			@Override
			public void run() {
				TempChart demo = new TempChart(TITLE);
				demo.pack();
				RefineryUtilities.centerFrameOnScreen(demo);
				demo.setVisible(true);
				demo.start();
			}
		});
	}
}